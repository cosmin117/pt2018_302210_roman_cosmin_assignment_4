import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



public class Controller {
    
	private View view;
	public Bank bank;
	public PersonWindow pw;
	public AccountWindow aw ;
	public DefaultTableModel model1 = new DefaultTableModel();
	public String[] header1 ={"id","name"};
	public DefaultTableModel model = new DefaultTableModel();
	public String[] header ={"id","balance","owner"};
	public Controller(View view) {
		this.view=view;
		this.view.personsListener(new Persons());
		this.view.accountsListener(new Accounts());
		 bank= new Bank();
		if(bank.aMap.isEmpty()== true)
		{
			bank.addPerson(new Person(1,"John"));
			bank.addPerson(new Person(2,"Timmy"));
			bank.addPerson(new Person(3,"Bob"));
			bank.addPerson(new Person(4,"Sally"));
			bank.addAccount(new SavingAccount(1,3600,"John"), bank.findPerson(1));
			bank.addAccount(new SpendingAccount(2,1000,"John"), bank.findPerson(1));
			bank.addAccount(new SavingAccount(3,100,"John"), bank.findPerson(1));
			bank.addAccount(new SpendingAccount(4,900,"Timmy"), bank.findPerson(2));
			bank.addAccount(new SpendingAccount(5,1000,"Bob"), bank.findPerson(3));
			bank.Save(bank.aMap);
		}
	}
	
	class Persons implements ActionListener {
		public void actionPerformed(ActionEvent e)
		{
			try {
			    pw=new PersonWindow();
				
	    		model1.setColumnIdentifiers(header1);
	    		for(Person i : bank.aMap.keySet())
	    		{	
	    			model1.addRow(new Object[] {i.getId(),i.getName()});
	    		}
				pw.table.setModel(model1);
				pw.addListener(new Add1());
				pw.editListener(new Edit1());
				pw.deleteListener(new Delete1());
			}
			catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
		}
	}
		class Accounts implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					aw = new AccountWindow();
					
		    		model.setColumnIdentifiers(header);
		    		for(Person i : bank.aMap.keySet())
		    		{
		    			for(Account j : bank.aMap.get(i))
		    			{
		    				model.addRow(new Object[] {j.getId(),j.getBalance(),j.getOwner()});
		    			}
		    		}
					aw.table.setModel(model);
					aw.addListener(new Add2());
					aw.add2Listener(new Add2Sv());
					aw.editListener(new Edit2());
					aw.deleteListener(new Delete2());
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Add1 implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					bank.addPerson(new Person(Integer.parseInt(pw.getId()),pw.getName()));
					pw.table = new JTable();
					for(Person i : bank.aMap.keySet())
		    		{	
		    			model1.addRow(new Object[] {i.getId(),i.getName()});
		    		}
					pw.table.setModel(model1);
					
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Edit1 implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					bank.editPerson(Integer.parseInt(pw.getIdo()),new Person(Integer.parseInt(pw.getId()),pw.getName()));
					pw.table = new JTable();
					for(Person i : bank.aMap.keySet())
		    		{	
		    			model1.addRow(new Object[] {i.getId(),i.getName()});
		    		}
					pw.table.setModel(model1);
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Delete1 implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					bank.removePerson(Integer.parseInt(pw.getId()));
					pw.table = new JTable();
					for(Person i : bank.aMap.keySet())
		    		{	
		    			model1.addRow(new Object[] {i.getId(),i.getName()});
		    		}
					pw.table.setModel(model1);
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Add2 implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {

					bank.addAccount(new SpendingAccount(Integer.parseInt(aw.getId()),Float.parseFloat(aw.getBalance()),bank.findPerson(Integer.parseInt(aw.getIdo())).getName()),bank.findPerson(Integer.parseInt(aw.getIdo())));
					pw.table = new JTable();
		    		model.setColumnIdentifiers(header);
		    		for(Person i : bank.aMap.keySet())
		    		{
		    			for(Account j : bank.aMap.get(i))
		    			{
		    				model.addRow(new Object[] {j.getId(),j.getBalance(),j.getOwner()});
		    			}
		    		}
					aw.table.setModel(model);
					
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Withdraw implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					bank.withdraw(Integer.parseInt(aw.getId()), Integer.parseInt(aw.getAmount()));
 					pw.table = new JTable();
		    		model.setColumnIdentifiers(header);
		    		for(Person i : bank.aMap.keySet())
		    		{
		    			for(Account j : bank.aMap.get(i))
		    			{
		    				model.addRow(new Object[] {j.getId(),j.getBalance(),j.getOwner()});
		    			}
		    		}
					aw.table.setModel(model);
					
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Deposit implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {

					bank.deposit(Integer.parseInt(aw.getId()), Integer.parseInt(aw.getAmount()));					
					pw.table = new JTable();
		    		model.setColumnIdentifiers(header);
		    		for(Person i : bank.aMap.keySet())
		    		{
		    			for(Account j : bank.aMap.get(i))
		    			{
		    				model.addRow(new Object[] {j.getId(),j.getBalance(),j.getOwner()});
		    			}
		    		}
					aw.table.setModel(model);
					
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Add2Sv implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {

					bank.addAccount(new SavingAccount(Integer.parseInt(aw.getId()),Float.parseFloat(aw.getBalance()),bank.findPerson(Integer.parseInt(aw.getIdo())).getName()),bank.findPerson(Integer.parseInt(aw.getIdo())));
					pw.table = new JTable();
		    		model.setColumnIdentifiers(header);
		    		for(Person i : bank.aMap.keySet())
		    		{
		    			for(Account j : bank.aMap.get(i))
		    			{
		    				model.addRow(new Object[] {j.getId(),j.getBalance(),j.getOwner()});
		    			}
		    		}
					aw.table.setModel(model);
					
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Edit2 implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
		class Delete2 implements ActionListener 
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					//bank.removeAccount(new SavingAccount(Integer.parseInt(aw.getId()),Float.parseFloat(aw.getBalance()),bank.findPerson(Integer.parseInt(aw.getIdo())).getName()),bank.findPerson(Integer.parseInt(aw.getIdo())));
					pw.table = new JTable();
		    		model.setColumnIdentifiers(header);
		    		for(Person i : bank.aMap.keySet())
		    		{
		    			for(Account j : bank.aMap.get(i))
		    			{
		    				model.addRow(new Object[] {j.getId(),j.getBalance(),j.getOwner()});
		    			}
		    		}
					aw.table.setModel(model);
				}
				catch (Exception e1) {JOptionPane.showMessageDialog(null, "Illegal input");}
			}
	
		}
}
