import javax.swing.JOptionPane;

public class SpendingAccount extends Account{
			
	
	public SpendingAccount(int id,float balance,String owner)
	{
		super(id,balance,owner);
	}
	
	public void deposit(int amount)
	{
		 super.setBalance(super.getBalance()+amount);
	}
	
	public float withdrawal(float amount)
	{
		 
		 if(super.getBalance()>=amount)
		 {
			 super.setBalance(super.getBalance()-amount);
			 JOptionPane.showMessageDialog(null, amount+" withdrawn");
			 return amount;
		 }
		 JOptionPane.showMessageDialog(null, "Insufficient funds!");
		 return 0;
	}
	
}
