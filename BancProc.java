
public interface BankProc {
		private Person findPerson(int id) {
			return null;
		}
		public void addPerson(Person pers);
		public void removePerson(int id);
		public void addAccount(Account acc,Person pers);
		public Account findAccount(int id) ;
		public void removeAccount(int id);
		public void editAccount(Account acc);
		public void deposit(float amount, int id) ;
		public void withdraw(int id, float amount) ;
}
