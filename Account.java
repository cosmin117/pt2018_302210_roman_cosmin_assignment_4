import javax.swing.JOptionPane;

public abstract class Account {
	
	private int id;
	private float balance;
	private String owner;
	
	public Account() {};
	
	public Account(int id,float balance,String owner)
	{
		this.id = id;
		this.balance = balance;
		this.owner = owner;
	}
	
	public int getId()
	{
		return id;
	}
	public float getBalance()
	{
		return balance;
	}

	public String getOwner()
	{
		return owner;
	}
	
	public void setBalance(float balance)
	{
		this.balance = balance;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}

	public void setOwner(String owner)
	{
		this.owner=owner;
	}
	public Number withdrawal(Number amount)
	{
		 return 0;
	}
	
	public void deposit(float amount)
	{
		
	}

}
