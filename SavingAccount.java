import javax.swing.JOptionPane;

public class SavingAccount extends Account {
		
		private float rate;
		private boolean deposit=true, withdrawal=true;
		
		public SavingAccount(int id,float balance,String owner)
		{
			super(id,balance,owner);
		}
		
		public void deposit(float amount)
		{
			if(deposit==true)
			{
				super.setBalance(amount);
				deposit = false;
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Cannot add money!");
			}
			
		}
		
		public float withdrawal()
		{
			float amount =0;
			if(withdrawal==true)
			{
				amount = super.getBalance();
				super.setBalance(0);
				deposit = false;
				
			}
			return amount;
		}
		
		public void computeInterest()
		{
			super.setBalance(super.getBalance()+super.getBalance()*rate);
		}
}
