
	import java.awt.event.ActionListener;

import javax.swing.*;
	
    public class View extends JFrame {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private JPanel panel ;
	private JButton persons;
	private JButton accounts;
	
	public View() {
		
		
		setName("tema4");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(640, 480);
		setLayout(null);
		persons = new JButton("persons");
		accounts = new JButton("accounts");
		persons.setBounds(200,180,100,60);
		accounts.setBounds(301,180,100,60);
        add(accounts);
        add(persons);
		this.setVisible(true);

	}
	public void personsListener(ActionListener al)
	{
		persons.addActionListener(al);
	}
	public void accountsListener(ActionListener al)
	{
		accounts.addActionListener(al);
	}
	


}
