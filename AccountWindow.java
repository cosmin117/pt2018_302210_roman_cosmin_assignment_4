import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
	
    public class AccountWindow extends JFrame {
	/**
		 * 
		 */
    	private static final long serialVersionUID = 1L;
    	public JTable table;
    	private  String[] header ={"id","balance","owner"};
    	private JButton edit,delete,add,add2,deposit,withdraw;
    	private JTextField id,ido,balance,owner,amount;
    	public AccountWindow() {
    		
    		
    		setName("account");
    		setSize(640, 480);
    		id = new JTextField("id");
    		ido = new JTextField("ido");
    		balance = new JTextField("balance");
    		owner = new JTextField("owner");
    		amount = new JTextField("amount");
    		edit = new JButton("edit");
    		delete = new JButton("del");
    		add = new JButton("add1");
    		add2 = new JButton("add2");
    		deposit = new JButton("dep");
    		withdraw = new JButton("wdr");
    		delete.setBounds(100,400,60,40);
    		add.setBounds(161,400,60,40);
    		edit.setBounds(222,400,60,40);
    		deposit.setBounds(283,400,60,40);
    		withdraw.setBounds(344,400,60,40);
    		add2.setBounds(405,400,60,40);
    		id.setBounds(100,359,80,40);
    		balance.setBounds(181,359,80,40);
    		owner.setBounds(262,359,80,40);
    		amount.setBounds(324,359,80,40);
    		ido.setBounds(405,359,80,40);
            add(add);
            add(delete);
            add(edit);
            add(id);
            add(balance);
            add(owner);
            add(ido);
            add(deposit);
            add(withdraw);
    		DefaultTableModel model = new DefaultTableModel();
    		model.setColumnIdentifiers(header);
    		table = new JTable(model);
    	    table.setBounds(160,30,400,250);
    	    JScrollPane scrollPane = new JScrollPane(table);
    	    scrollPane.setBounds(36, 37, 407, 79);
    	    getContentPane().add(scrollPane);
    		setLayout(null);
    		this.setVisible(true);

    	}
    	public void addListener(ActionListener al)
    	{
    		add.addActionListener(al);
    	}
    	public void add2Listener(ActionListener al)
    	{
    		add2.addActionListener(al);
    	}
    	public void deleteListener(ActionListener al)
    	{
    		delete.addActionListener(al);
    	}
    	public void editListener(ActionListener al)
    	{
    		edit.addActionListener(al);
    	}
    	
    	public void depListener(ActionListener al)
    	{
    		deposit.addActionListener(al);
    	}
    	public void withListener(ActionListener al)
    	{
    		withdraw.addActionListener(al);
    	}
    	
    	public String getId()
    	{
    		return id.getText();
    	}
    	public String getIdo()
    	{
    		return ido.getText();
    	}
    	public String getBalance()
    	{
    		return balance.getText();
    	}
    	public String getowner()
    	{
    		return owner.getText();
    	}
    	public String getAmount()
    	{
    		return amount.getText();
    	}

}
