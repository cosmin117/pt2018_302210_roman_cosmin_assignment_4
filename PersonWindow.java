
	import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
	
    public class PersonWindow extends JFrame {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	public JTable table;
	private  String[] header ={"id","name"};
	private JButton edit,delete,add;
	private JTextField id,name,ido;
	public PersonWindow() {
		
		
		setName("persons");

		setSize(640, 480);
		ido = new JTextField("ido");
		id = new JTextField("id");
		name = new JTextField("name");
		edit = new JButton("edit");
		delete = new JButton("del");
		add = new JButton("add");
		delete.setBounds(100,400,60,40);
		add.setBounds(161,400,60,40);
		edit.setBounds(222,400,60,40);
		id.setBounds(100,359,80,40);
		name.setBounds(181,359,80,40);
		ido.setBounds(262,359,80,40);
		
        add(add);
        add(delete);
        add(edit);
        add(id);
        add(ido);
        add(name);
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(header);
		table = new JTable(model);
	    table.setBounds(160,30,400,250);
	    JScrollPane scrollPane = new JScrollPane(table);
	    scrollPane.setBounds(36, 37, 407, 79);
	    getContentPane().add(scrollPane);
		setLayout(null);
		this.setVisible(true);

	}
	public void addListener(ActionListener al)
	{
		add.addActionListener(al);
	}
	public void deleteListener(ActionListener al)
	{
		delete.addActionListener(al);
	}
	public void editListener(ActionListener al)
	{
		edit.addActionListener(al);
	}
	public String getId()
	{
		return id.getText();
	}
	public String getIdo()
	{
		return ido.getText();
	}
	public String getName()
	{
		return name.getText();
	}


}
