
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;


public class Bank implements BankProc {

	public HashMap<Person, Set<Account>> aMap;

	
	
	public Bank() {
		aMap = new HashMap<Person, Set<Account>>();
		
	}
	
	
	private boolean personExists(Person pers)
	{   assert(validPerson(pers));
		for(Person i: aMap.keySet()) {
			if (i.getId()== pers.getId() ) {
				return true;
			}	
		}
		return false;
	}
	public Person findPerson(int id)
	{
		for(Person i: aMap.keySet()) {
			if (i.getId()== id ) {
				return i;
			}	
		}
		return null;
	}

	public void addPerson(Person pers) {
		 assert(validPerson(pers));
		if(this.personExists(pers)==true)
		{
			JOptionPane.showMessageDialog(null, "The person is alredy registered!");
		}
		else
		{
			Set <Account> accSet = new HashSet<Account>();
			aMap.put(pers, accSet);
		}
		
	}


	public void removePerson(int id) {
		if(findPerson(id)==null)
		{
			JOptionPane.showMessageDialog(null, "The person does not exist!");
		}
		else
		{
			aMap.remove(findPerson(id), aMap.get(findPerson(id)));
		}
	}


	public void addAccount(Account acc,Person pers) {
		 assert(validPerson(pers));
		 assert(validAccount(acc));
		if(aMap.containsKey(pers))
		{
			aMap.get(pers).add(acc);
		}
		else
			JOptionPane.showMessageDialog(null, "The person doesn't exist!");
	}
	
	public Account findAccount(int id) {
		for(Person i : aMap.keySet() )
			for(Account j :aMap.get(i))
				if(j.getId()==id)
				{
					return j;
				}
		JOptionPane.showMessageDialog(null, "The account does not exist!");	
		return null;
	}


	public void removeAccount(int id) {
		
		for(Person i : aMap.keySet() )
			for(Account j :aMap.get(i))
				if(j.getId()==id)
				{
					aMap.get(i).remove(j);
					break;
				}
	}
	
	
	public void editAccount(Account acc) {
		assert(validAccount(acc));
		for(Person p : aMap.keySet() )
			for(Account a :aMap.get(p))
				if(a.getId()==acc.getId())
				{
					aMap.get(p).remove(a);
					aMap.get(p).add(acc);
				}
	}
	
	public void deposit(float amount, int id) {
		for(Person i : aMap.keySet() )
			for(Account j :aMap.get(i))
				
				if(j.getId()==id)
				{
					j.deposit(amount);
				}
	}

	public void withdraw(int id, float amount) {

		for(Person i : aMap.keySet() )
			for(Account j :aMap.get(i))
				if(j.getId()==id)
				{
					if(j.getClass()==SpendingAccount.class)
					{
						j.withdrawal(amount);
					}
					else
					{
						j.withdrawal(amount);
					}
				}
		
	}
	public void Save(HashMap<Person, Set<Account>> aMap) {
		try{
			File file=new File("save.txt");
			FileOutputStream fileOs = new FileOutputStream(file);
			ObjectOutputStream objectOs = new ObjectOutputStream(fileOs);
			objectOs.writeObject(aMap);
			objectOs.close();
			fileOs.close();
		}
		catch(IOException e){
			
		}
	}
	@SuppressWarnings("unchecked")
	public  HashMap<Person,Set<Account>> LoadDate(){
		
		HashMap<Person,Set<Account>> aMap = new HashMap<Person,Set<Account>>();
		
		try{
			File file=new File("Save.txt");
			FileInputStream fileIs = new FileInputStream(file);
			ObjectInputStream objectOs = new ObjectInputStream(fileIs);
			aMap = (HashMap<Person, Set<Account>>) objectOs.readObject();
			objectOs.close();
			fileIs.close();
		}
		catch(Exception e){
			
		}
		
		return aMap;
	}
		public void editPerson(int id, Person p) {
			 assert(validPerson(p));
		for(Person i: aMap.keySet()) {
			if (i.getId() == id) {
					i.setId(p.getId());
					i.setName(p.getName());
			
			}	
		}
	}
		public boolean validPerson(Person pers) {
			if(pers.getId()>=0&&pers.getName()!=null)
				return true;
			else
				return false;
		}
		public boolean validAccount(Account acc) {
			if(acc.getId()>=0)
				return true;
			else
				return false;
		}
		

}
